package br.com.itau.investimento.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Transacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;
import br.com.itau.investimento.repositories.TransacaoRepository;
import br.com.itau.investimento.viewobjects.Cliente;
import br.com.itau.investimento.viewobjects.Investimento;
import br.com.itau.investimento.viewobjects.Produto;

@Service
public class AplicacaoService {
	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	TransacaoRepository transacaoRepository;
	
	RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
	EurekaClient eurekaClient;
	
	public Optional<Investimento> criar(Investimento investimento) {
		InstanceInfo clienteInfo = eurekaClient.getNextServerFromEureka("cliente", false);
		String clienteUrl = clienteInfo.getHomePageUrl();
		
		Cliente cliente = restTemplate.getForObject(clienteUrl + investimento.getCliente().getCpf(), Cliente.class);
		investimento.setCliente(cliente);
	
		InstanceInfo produtoInfo = eurekaClient.getNextServerFromEureka("produto", false);
		String produtoUrl = produtoInfo.getHomePageUrl();
		
		Produto produto = restTemplate.getForObject(produtoUrl + investimento.getProduto().getId(), Produto.class);
		investimento.setProduto(produto);
		
		Aplicacao aplicacao = salvarAplicacao(investimento);
		investimento.setAplicacao(aplicacao);
		
		Transacao transacao = salvarTransacao(investimento);
		investimento.setTransacao(transacao);
		
		return Optional.of(investimento);
	}
		
	private Aplicacao salvarAplicacao(Investimento investimento) {
		Aplicacao aplicacao = new Aplicacao();
		
		aplicacao.setCliente(investimento.getCliente());
		aplicacao.setProduto(investimento.getProduto());
		aplicacao.setSaldo(investimento.getTransacao().getValor());
		aplicacao.setDataCriacao(LocalDate.now());
		
		return aplicacaoRepository.save(aplicacao);
	}
	
	private Transacao salvarTransacao(Investimento investimento) {
		Transacao transacao = investimento.getTransacao();
		
		transacao.setId(UUID.randomUUID());
		transacao.setAplicacao(investimento.getAplicacao());
		transacao.setTimestamp(LocalDateTime.now());
		
		return transacaoRepository.save(transacao);
	}
}
